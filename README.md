# Preseed creator

Small script aimed to ease the creation of a preseeded Debian ISO image

## Prerequistes

In order to run the script, you need to install these packages:

```
apt install wget genisoimage xorriso isolinux
```

## How to use

Some packages like `genisoimage` need to be installed. Check the script to know which packages if it fails.

The script needs to be run as root.

`./preseed_creator.sh -h` will teach you how to use it.

## License

Released under the terms of the WTFPL

## Author

Luc Didry, <https://fiat-tux.fr>

## Contributor

[@adriengg](https://framagit.org/adriengg) for the hybrid iso part

## Make a donation

You can make a donation to the author on [Tipeee](https://www.tipeee.com/fiat-tux) or on [Liberapay](https://liberapay.com/sky/).
